# Gilded Rose Refactoring Kata
##Java Implementation.
The kata is located in 'GildedRose Kata' directory.

## Implementing in Spring boot

Located in 'GildedRose Spring' directory.

Requirements for dev environment:
```
* java 1.8
* docker
* docker-compose
```

To launch a container with Elasticsearch run:
```
docker-compose up
```

if you need fixtures do a POST request without a body to:
```
http://localhost:8080/fixtures
```