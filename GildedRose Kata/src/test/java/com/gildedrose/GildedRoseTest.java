package com.gildedrose;

import com.gildedrose.builder.ItemBuilder;
import com.gildedrose.domain.Item;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GildedRoseTest {

    @Test
    public void shouldNotDegradeSulfuras() {
        final ItemBuilder sulfurasBuilder = ItemBuilder.anItem()
                .withName("Sulfuras, Hand of Ragnaros")
                .withSellIn(0)
                .withQuality(80);

        final int expectedSellInChange = 0;
        final int expectedQualityChange = 0;

        assertItemUpdate(sulfurasBuilder, expectedSellInChange, expectedQualityChange);
    }

    @Test
    public void shouldUpgradeAgedBrie() {
        final ItemBuilder agedBrieBuilder = ItemBuilder.anItem()
                .withName("Aged Brie")
                .withSellIn(2)
                .withQuality(0);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = 1;

        assertItemUpdate(agedBrieBuilder, expectedSellInChange, expectedQualityChange);
    }

    @Test
    public void shouldNotUpgradeAgedBrieAbove50() {
        final ItemBuilder agedBrieBuilder = ItemBuilder.anItem()
                .withName("Aged Brie")
                .withSellIn(0)
                .withQuality(50);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = 0;

        assertItemUpdate(agedBrieBuilder, expectedSellInChange, expectedQualityChange);
    }

    @Test
    public void shouldDegradeStandardItem() {
        final ItemBuilder dexterityVestBuilder = ItemBuilder.anItem()
                .withName("+5 Dexterity Vest")
                .withSellIn(10)
                .withQuality(20);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = -1;

        assertItemUpdate(dexterityVestBuilder, expectedSellInChange, expectedQualityChange);
    }

    @Test
    public void shouldDegradeStandardItemAfterSellIn() {
        final ItemBuilder dexterityVestBuilder = ItemBuilder.anItem()
                .withName("+5 Dexterity Vest")
                .withSellIn(0)
                .withQuality(10);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = -2;

        assertItemUpdate(dexterityVestBuilder, expectedSellInChange, expectedQualityChange);
    }

    @Test
    public void shouldUpgradeAgedBrieAfterSellIn() {
        final ItemBuilder agedBrieBuilder = ItemBuilder.anItem()
                .withName("Aged Brie")
                .withSellIn(0)
                .withQuality(20);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = 2;

        assertItemUpdate(agedBrieBuilder, expectedSellInChange, expectedQualityChange);
    }

    @Test
    public void shouldNotDegradeQualityBelowZero() {
        final ItemBuilder dexterityVestBuilder = ItemBuilder.anItem()
                .withName("+5 Dexterity Vest")
                .withSellIn(0)
                .withQuality(0);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = 0;

        assertItemUpdate(dexterityVestBuilder, expectedSellInChange, expectedQualityChange);
    }

    @Test
    public void shouldUpgradeBackstagePassBy1() {
        final ItemBuilder backstagePassBuilder = ItemBuilder.anItem()
                .withName("Backstage passes to a TAFKAL80ETC concert")
                .withSellIn(15)
                .withQuality(20);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = 1;

        assertItemUpdate(backstagePassBuilder, expectedSellInChange, expectedQualityChange);
    }

    @Test
    public void shouldUpgradeBackstagePassBy2() {
        final ItemBuilder backstagePassBuilder = ItemBuilder.anItem()
                .withName("Backstage passes to a TAFKAL80ETC concert")
                .withSellIn(10)
                .withQuality(20);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = 2;

        assertItemUpdate(backstagePassBuilder, expectedSellInChange, expectedQualityChange);
    }

    @Test
    public void shouldUpgradeBackstagePassBy3() {
        final ItemBuilder backstagePassBuilder = ItemBuilder.anItem()
                .withName("Backstage passes to a TAFKAL80ETC concert")
                .withSellIn(5)
                .withQuality(20);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = 3;

        assertItemUpdate(backstagePassBuilder, expectedSellInChange, expectedQualityChange);
    }

    @Test
    public void shouldNotUpgradeBackstagePassAbove50() {
        final ItemBuilder backstagePassBuilder = ItemBuilder.anItem()
                .withName("Backstage passes to a TAFKAL80ETC concert")
                .withSellIn(5)
                .withQuality(50);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = 0;

        assertItemUpdate(backstagePassBuilder, expectedSellInChange, expectedQualityChange);
    }

    @Test
    public void shouldDropBackstagePassQualityTo0() {
        final ItemBuilder backstagePassBuilder = ItemBuilder.anItem()
                .withName("Backstage passes to a TAFKAL80ETC concert")
                .withSellIn(0)
                .withQuality(20);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = -20;

        assertItemUpdate(backstagePassBuilder, expectedSellInChange, expectedQualityChange);
    }

    @Test
    public void shouldDegradeConjuredItemBy2() {
        final ItemBuilder conjuredItemBuilder = ItemBuilder.anItem()
                .withName("Conjured Mana Cake")
                .withSellIn(10)
                .withQuality(20);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = -2;

        assertItemUpdate(conjuredItemBuilder, expectedSellInChange, expectedQualityChange);
    }

    @Test
    public void shouldDegradeConjuredItemBy4() {
        final ItemBuilder conjuredItemBuilder = ItemBuilder.anItem()
                .withName("Conjured Mana Cake")
                .withSellIn(0)
                .withQuality(20);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = -4;

        assertItemUpdate(conjuredItemBuilder, expectedSellInChange, expectedQualityChange);
    }

    private void assertItemUpdate(
            final ItemBuilder itemBuilder,
            final int sellInChange,
            final int qualityChange
    ) {
        final Item itemBefore = itemBuilder.build();

        final Item[] items = new Item[]{itemBuilder.build()};
        final GildedRose app = new GildedRose(items);
        app.updateQuality();

        assertEquals(itemBefore.name, app.items[0].name);
        assertEquals(itemBefore.sellIn + sellInChange, app.items[0].sellIn);
        assertEquals(itemBefore.quality + qualityChange, app.items[0].quality);
    }

}
