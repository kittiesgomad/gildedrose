package com.gildedrose.builder;

import com.gildedrose.domain.Item;

public class ItemBuilder {

    private String name;

    private int sellIn;

    private int quality;

    public ItemBuilder() {
    }

    public static ItemBuilder anItem() {
        return new ItemBuilder();
    }

    public ItemBuilder withName(final String name) {
        this.name = name;
        return this;
    }

    public ItemBuilder withSellIn(final int sellIn) {
        this.sellIn = sellIn;
        return this;
    }

    public ItemBuilder withQuality(final int quality) {
        this.quality = quality;
        return this;
    }

    public Item build() {
        return new Item(name, sellIn, quality);
    }
}
