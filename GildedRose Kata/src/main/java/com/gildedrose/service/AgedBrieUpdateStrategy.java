package com.gildedrose.service;

import com.gildedrose.domain.Item;
import com.gildedrose.domain.ItemType;

public final class AgedBrieUpdateStrategy implements ItemUpdateStrategy {

    public boolean isValid(final Item item) {
        return ItemType.ofItem(item).equals(ItemType.AGED_BRIE) && item.quality <= ItemType.AGED_BRIE.maxQuality;
    }
}
