package com.gildedrose.service;

import com.gildedrose.domain.Item;
import com.gildedrose.domain.ItemType;

import javax.xml.bind.ValidationException;

public interface ItemUpdateStrategy {

    boolean isValid(final Item item);

    default void handleSellIn(final Item item) {
        item.sellIn = item.sellIn + ItemType.ofItem(item).sellInStep;
    }

    default void handleQuality(final Item item) {
        if (item.sellIn < 0) {
            item.quality = item.quality + ItemType.ofItem(item).qualityStep;
        }
        item.quality = item.quality + ItemType.ofItem(item).qualityStep;
    }

    default void handleMaxValues(final Item item) {
        if (item.quality < ItemType.ofItem(item).minQuality) {
            item.quality = ItemType.ofItem(item).minQuality;
        }
        if (item.quality > ItemType.ofItem(item).maxQuality) {
            item.quality = ItemType.ofItem(item).maxQuality;
        }
    }

    default void update(final Item item) throws ValidationException {
        if (!isValid(item)) {
            throw new ValidationException(String.format("Invalid item %s", item.name));
        }

        handleSellIn(item);
        handleQuality(item);
        handleMaxValues(item);
    }
}
