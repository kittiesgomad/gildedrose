package com.gildedrose.service;

import com.gildedrose.domain.Item;
import com.gildedrose.domain.ItemType;

public final class BackstagePassUpdateStrategy implements ItemUpdateStrategy {

    public boolean isValid(final Item item) {
        return ItemType.ofItem(item).equals(ItemType.BACKSTAGE_PASS)
                && item.quality <= ItemType.BACKSTAGE_PASS.maxQuality;
    }

    public void handleQuality(final Item item) {
        if (item.sellIn < 0) {
            item.quality = 0;
            return;
        }
        if (item.sellIn <= 10) {
            item.quality = item.quality + ItemType.BACKSTAGE_PASS.qualityStep;
        }
        if (item.sellIn <= 5) {
            item.quality = item.quality + ItemType.BACKSTAGE_PASS.qualityStep;
        }
        item.quality = item.quality + ItemType.BACKSTAGE_PASS.qualityStep;
    }
}
