package com.gildedrose.service;

import com.gildedrose.domain.Item;
import com.gildedrose.domain.ItemType;

public final class SulfurasUpdateStrategy implements ItemUpdateStrategy {

    public boolean isValid(final Item item) {
        return ItemType.ofItem(item).equals(ItemType.SULFURAS) && item.quality == ItemType.SULFURAS.maxQuality;
    }
}
