package com.gildedrose.service;

import com.gildedrose.domain.Item;
import com.gildedrose.domain.ItemType;

public final class ConjuredItemUpdateStrategy implements ItemUpdateStrategy {

    public boolean isValid(final Item item) {
        return ItemType.ofItem(item).equals(ItemType.CONJURED) && item.quality <= ItemType.CONJURED.maxQuality;
    }
}
