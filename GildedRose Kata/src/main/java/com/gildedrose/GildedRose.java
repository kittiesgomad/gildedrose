package com.gildedrose;

import com.gildedrose.domain.Item;
import com.gildedrose.domain.ItemType;
import com.gildedrose.service.*;

import javax.xml.bind.ValidationException;
import java.util.stream.Stream;

class GildedRose {
    Item[] items;

    private final StandardItemUpdateStrategy standardItemUpdateStrategy = new StandardItemUpdateStrategy();
    private final AgedBrieUpdateStrategy agedBrieUpdateStrategy = new AgedBrieUpdateStrategy();
    private final BackstagePassUpdateStrategy backstagePassUpdateStrategy = new BackstagePassUpdateStrategy();
    private final SulfurasUpdateStrategy sulfurasUpdateStrategy = new SulfurasUpdateStrategy();
    private final ConjuredItemUpdateStrategy conjuredItemUpdateStrategy = new ConjuredItemUpdateStrategy();

    public GildedRose(final Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        Stream.of(items).parallel()
                .forEach(this::updateItem);
    }

    private void updateItem(final Item item) {
        final ItemType type = ItemType.ofItem(item);
        try {
            updateStrategyForType(type).update(item);
        } catch (final ValidationException e) {
            System.out.println(e.getMessage());
        }
    }

    private ItemUpdateStrategy updateStrategyForType(final ItemType type) {
        switch (type) {
            case SULFURAS:
                return sulfurasUpdateStrategy;
            case AGED_BRIE:
                return agedBrieUpdateStrategy;
            case BACKSTAGE_PASS:
                return backstagePassUpdateStrategy;
            case CONJURED:
                return conjuredItemUpdateStrategy;
            default:
                return standardItemUpdateStrategy;
        }
    }
}