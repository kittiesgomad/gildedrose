package com.gildedrose.spring.service;

import com.gildedrose.spring.domain.Item;
import com.gildedrose.spring.domain.builder.ItemBuilder;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.ValidationException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class AgedBrieUpdateStrategyTest {

    private AgedBrieUpdateStrategy agedBrieUpdateStrategy;

    @Before
    public void setUp() {
        agedBrieUpdateStrategy = new AgedBrieUpdateStrategy();
    }

    @Test
    public void shouldNotValidateStandardItem() {
        final ItemBuilder standardItemBuilder = ItemBuilder.anItem()
                .withName("Standard");

        assertFalse(agedBrieUpdateStrategy.isValid(standardItemBuilder.build()));
    }

    @Test
    public void shouldUpgradeAgedBrie() throws ValidationException {
        final ItemBuilder agedBrieBuilder = ItemBuilder.anItem()
                .withName("Aged Brie")
                .withSellIn(2)
                .withQuality(0);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = 1;

        assertItemUpdate(agedBrieBuilder, expectedSellInChange, expectedQualityChange);
    }

    @Test
    public void shouldNotUpgradeAgedBrieAbove50() throws ValidationException {
        final ItemBuilder agedBrieBuilder = ItemBuilder.anItem()
                .withName("Aged Brie")
                .withSellIn(0)
                .withQuality(50);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = 0;

        assertItemUpdate(agedBrieBuilder, expectedSellInChange, expectedQualityChange);
    }

    @Test
    public void shouldUpgradeAgedBrieAfterSellIn() throws ValidationException {
        final ItemBuilder agedBrieBuilder = ItemBuilder.anItem()
                .withName("Aged Brie")
                .withSellIn(0)
                .withQuality(20);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = 2;

        assertItemUpdate(agedBrieBuilder, expectedSellInChange, expectedQualityChange);
    }

    private void assertItemUpdate(
            final ItemBuilder itemBuilder,
            final int sellInChange,
            final int qualityChange
    ) throws ValidationException {
        final Item itemBefore = itemBuilder.build();
        final Item itemAfter = itemBuilder.build();
        agedBrieUpdateStrategy.update(itemAfter);

        assertEquals(itemBefore.name, itemAfter.name);
        assertEquals(itemBefore.sellIn + sellInChange, itemAfter.sellIn);
        assertEquals(itemBefore.quality + qualityChange, itemAfter.quality);
    }
}
