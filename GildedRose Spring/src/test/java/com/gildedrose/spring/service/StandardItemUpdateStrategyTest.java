package com.gildedrose.spring.service;

import com.gildedrose.spring.domain.Item;
import com.gildedrose.spring.domain.builder.ItemBuilder;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.ValidationException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class StandardItemUpdateStrategyTest {

    private StandardItemUpdateStrategy standardItemUpdateStrategy;

    @Before
    public void setUp() {
        standardItemUpdateStrategy = new StandardItemUpdateStrategy();
    }

    @Test
    public void shouldNotValidateSulfurasItem() {
        final ItemBuilder sulfurasItemBuilder = ItemBuilder.anItem()
                .withName("Sulfuras");

        assertFalse(standardItemUpdateStrategy.isValid(sulfurasItemBuilder.build()));
    }

    @Test
    public void shouldDegradeStandardItem() throws ValidationException {
        final ItemBuilder dexterityVestBuilder = ItemBuilder.anItem()
                .withName("+5 Dexterity Vest")
                .withSellIn(10)
                .withQuality(20);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = -1;

        assertItemUpdate(dexterityVestBuilder, expectedSellInChange, expectedQualityChange);
    }

    @Test
    public void shouldDegradeStandardItemAfterSellIn() throws ValidationException {
        final ItemBuilder dexterityVestBuilder = ItemBuilder.anItem()
                .withName("+5 Dexterity Vest")
                .withSellIn(0)
                .withQuality(10);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = -2;

        assertItemUpdate(dexterityVestBuilder, expectedSellInChange, expectedQualityChange);
    }

    @Test
    public void shouldNotDegradeQualityBelowZero() throws ValidationException {
        final ItemBuilder dexterityVestBuilder = ItemBuilder.anItem()
                .withName("+5 Dexterity Vest")
                .withSellIn(0)
                .withQuality(0);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = 0;

        assertItemUpdate(dexterityVestBuilder, expectedSellInChange, expectedQualityChange);
    }

    private void assertItemUpdate(
            final ItemBuilder itemBuilder,
            final int sellInChange,
            final int qualityChange
    ) throws ValidationException {
        final Item itemBefore = itemBuilder.build();
        final Item itemAfter = itemBuilder.build();
        standardItemUpdateStrategy.update(itemAfter);

        assertEquals(itemBefore.name, itemAfter.name);
        assertEquals(itemBefore.sellIn + sellInChange, itemAfter.sellIn);
        assertEquals(itemBefore.quality + qualityChange, itemAfter.quality);
    }
}
