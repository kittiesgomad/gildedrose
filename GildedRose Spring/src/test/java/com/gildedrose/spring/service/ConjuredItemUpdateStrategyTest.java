package com.gildedrose.spring.service;

import com.gildedrose.spring.domain.Item;
import com.gildedrose.spring.domain.builder.ItemBuilder;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.ValidationException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class ConjuredItemUpdateStrategyTest {

    private ConjuredItemUpdateStrategy conjuredItemUpdateStrategy;

    @Before
    public void setUp() {
        conjuredItemUpdateStrategy = new ConjuredItemUpdateStrategy();
    }

    @Test
    public void shouldNotValidateStandardItem() {
        final ItemBuilder standardItemBuilder = ItemBuilder.anItem()
                .withName("Standard");

        assertFalse(conjuredItemUpdateStrategy.isValid(standardItemBuilder.build()));
    }

    @Test
    public void shouldDegradeConjuredItemBy2() throws ValidationException {
        final ItemBuilder conjuredItemBuilder = ItemBuilder.anItem()
                .withName("Conjured Mana Cake")
                .withSellIn(10)
                .withQuality(20);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = -2;

        assertItemUpdate(conjuredItemBuilder, expectedSellInChange, expectedQualityChange);
    }

    @Test
    public void shouldDegradeConjuredItemBy4() throws ValidationException {
        final ItemBuilder conjuredItemBuilder = ItemBuilder.anItem()
                .withName("Conjured Mana Cake")
                .withSellIn(0)
                .withQuality(20);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = -4;

        assertItemUpdate(conjuredItemBuilder, expectedSellInChange, expectedQualityChange);
    }

    private void assertItemUpdate(
            final ItemBuilder itemBuilder,
            final int sellInChange,
            final int qualityChange
    ) throws ValidationException {
        final Item itemBefore = itemBuilder.build();
        final Item itemAfter = itemBuilder.build();
        conjuredItemUpdateStrategy.update(itemAfter);

        assertEquals(itemBefore.name, itemAfter.name);
        assertEquals(itemBefore.sellIn + sellInChange, itemAfter.sellIn);
        assertEquals(itemBefore.quality + qualityChange, itemAfter.quality);
    }
}
