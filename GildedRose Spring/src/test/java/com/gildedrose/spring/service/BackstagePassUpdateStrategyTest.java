package com.gildedrose.spring.service;

import com.gildedrose.spring.domain.Item;
import com.gildedrose.spring.domain.builder.ItemBuilder;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.ValidationException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class BackstagePassUpdateStrategyTest {

    private BackstagePassUpdateStrategy backstagePassUpdateStrategy;

    @Before
    public void setUp() {
        backstagePassUpdateStrategy = new BackstagePassUpdateStrategy();
    }

    @Test
    public void shouldNotValidateStandardItem() {
        final ItemBuilder standardItemBuilder = ItemBuilder.anItem()
                .withName("Standard");

        assertFalse(backstagePassUpdateStrategy.isValid(standardItemBuilder.build()));
    }


    @Test
    public void shouldUpgradeBackstagePassBy1() throws ValidationException {
        final ItemBuilder backstagePassBuilder = ItemBuilder.anItem()
                .withName("Backstage passes to a TAFKAL80ETC concert")
                .withSellIn(15)
                .withQuality(20);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = 1;

        assertItemUpdate(backstagePassBuilder, expectedSellInChange, expectedQualityChange);
    }

    @Test
    public void shouldUpgradeBackstagePassBy2() throws ValidationException {
        final ItemBuilder backstagePassBuilder = ItemBuilder.anItem()
                .withName("Backstage passes to a TAFKAL80ETC concert")
                .withSellIn(10)
                .withQuality(20);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = 2;

        assertItemUpdate(backstagePassBuilder, expectedSellInChange, expectedQualityChange);
    }

    @Test
    public void shouldUpgradeBackstagePassBy3() throws ValidationException {
        final ItemBuilder backstagePassBuilder = ItemBuilder.anItem()
                .withName("Backstage passes to a TAFKAL80ETC concert")
                .withSellIn(5)
                .withQuality(20);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = 3;

        assertItemUpdate(backstagePassBuilder, expectedSellInChange, expectedQualityChange);
    }

    @Test
    public void shouldNotUpgradeBackstagePassAbove50() throws ValidationException {
        final ItemBuilder backstagePassBuilder = ItemBuilder.anItem()
                .withName("Backstage passes to a TAFKAL80ETC concert")
                .withSellIn(5)
                .withQuality(50);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = 0;

        assertItemUpdate(backstagePassBuilder, expectedSellInChange, expectedQualityChange);
    }

    @Test
    public void shouldDropBackstagePassQualityTo0() throws ValidationException {
        final ItemBuilder backstagePassBuilder = ItemBuilder.anItem()
                .withName("Backstage passes to a TAFKAL80ETC concert")
                .withSellIn(0)
                .withQuality(20);

        final int expectedSellInChange = -1;
        final int expectedQualityChange = -20;

        assertItemUpdate(backstagePassBuilder, expectedSellInChange, expectedQualityChange);
    }

    private void assertItemUpdate(
            final ItemBuilder itemBuilder,
            final int sellInChange,
            final int qualityChange
    ) throws ValidationException {
        final Item itemBefore = itemBuilder.build();
        final Item itemAfter = itemBuilder.build();
        backstagePassUpdateStrategy.update(itemAfter);

        assertEquals(itemBefore.name, itemAfter.name);
        assertEquals(itemBefore.sellIn + sellInChange, itemAfter.sellIn);
        assertEquals(itemBefore.quality + qualityChange, itemAfter.quality);
    }
}
