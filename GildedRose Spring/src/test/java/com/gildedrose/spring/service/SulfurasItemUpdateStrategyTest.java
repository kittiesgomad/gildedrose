package com.gildedrose.spring.service;

import com.gildedrose.spring.domain.Item;
import com.gildedrose.spring.domain.builder.ItemBuilder;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.ValidationException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class SulfurasItemUpdateStrategyTest {

    private SulfurasUpdateStrategy sulfurasUpdateStrategy;

    @Before
    public void setUp() {
        sulfurasUpdateStrategy = new SulfurasUpdateStrategy();
    }

    @Test
    public void shouldNotValidateSulfurasItem() {
        final ItemBuilder sulfurasItemBuilder = ItemBuilder.anItem()
                .withName("Sulfuras");

        assertFalse(sulfurasUpdateStrategy.isValid(sulfurasItemBuilder.build()));
    }

    @Test
    public void shouldNotDegradeSulfuras() throws ValidationException {
        final ItemBuilder sulfurasBuilder = ItemBuilder.anItem()
                .withName("Sulfuras, Hand of Ragnaros")
                .withSellIn(0)
                .withQuality(80);

        final int expectedSellInChange = 0;
        final int expectedQualityChange = 0;

        assertItemUpdate(sulfurasBuilder, expectedSellInChange, expectedQualityChange);
    }

    private void assertItemUpdate(
            final ItemBuilder itemBuilder,
            final int sellInChange,
            final int qualityChange
    ) throws ValidationException {
        final Item itemBefore = itemBuilder.build();
        final Item itemAfter = itemBuilder.build();
        sulfurasUpdateStrategy.update(itemAfter);

        assertEquals(itemBefore.name, itemAfter.name);
        assertEquals(itemBefore.sellIn + sellInChange, itemAfter.sellIn);
        assertEquals(itemBefore.quality + qualityChange, itemAfter.quality);
    }
}
