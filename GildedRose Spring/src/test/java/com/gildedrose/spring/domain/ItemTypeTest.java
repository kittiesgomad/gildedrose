package com.gildedrose.spring.domain;

import com.gildedrose.spring.domain.builder.ItemBuilder;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ItemTypeTest {

    @Test
    public void shouldReturnAgedBrie() {
        final ItemBuilder itemBuilder = ItemBuilder.anItem()
                .withName("Aged Brie");

        assertEquals(ItemType.ofItem(itemBuilder.build()), ItemType.AGED_BRIE);
    }

    @Test
    public void shouldReturnBackstagePass() {
        final ItemBuilder itemBuilder = ItemBuilder.anItem()
                .withName("Backstage");

        assertEquals(ItemType.ofItem(itemBuilder.build()), ItemType.BACKSTAGE_PASS);
    }

    @Test
    public void shouldReturnConjured() {
        final ItemBuilder itemBuilder = ItemBuilder.anItem()
                .withName("Conjured");

        assertEquals(ItemType.ofItem(itemBuilder.build()), ItemType.CONJURED);
    }

    @Test
    public void shouldReturnStandard() {
        final ItemBuilder itemBuilder = ItemBuilder.anItem()
                .withName("Elixir");

        assertEquals(ItemType.ofItem(itemBuilder.build()), ItemType.STANDARD);
    }

    @Test
    public void shouldReturnSulfuras() {
        final ItemBuilder itemBuilder = ItemBuilder.anItem()
                .withName("Sulfuras");

        assertEquals(ItemType.ofItem(itemBuilder.build()), ItemType.SULFURAS);
    }
}
