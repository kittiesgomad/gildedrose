package com.gildedrose.spring.scheduled;

import com.gildedrose.spring.domain.Item;
import com.gildedrose.spring.domain.builder.ItemBuilder;
import com.gildedrose.spring.repository.ItemRepository;
import com.gildedrose.spring.service.*;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.ValidationException;
import java.util.Collections;

import static org.mockito.Mockito.*;

public class ItemUpdateScheduledTaskTest {

    private final ItemRepository itemRepository = mock(ItemRepository.class);
    private final AgedBrieUpdateStrategy agedBrieUpdateStrategy = mock(AgedBrieUpdateStrategy.class);
    private final BackstagePassUpdateStrategy backstagePassUpdateStrategy = mock(BackstagePassUpdateStrategy.class);
    private final ConjuredItemUpdateStrategy conjuredItemUpdateStrategy = mock(ConjuredItemUpdateStrategy.class);
    private final StandardItemUpdateStrategy standardItemUpdateStrategy = mock(StandardItemUpdateStrategy.class);
    private final SulfurasUpdateStrategy sulfurasUpdateStrategy = mock(SulfurasUpdateStrategy.class);

    private ItemUpdateScheduledTask itemUpdateScheduledTask;

    private final Item agedBrie = ItemBuilder.anItem().withName("Aged Brie").build();
    private final Item backstage = ItemBuilder.anItem().withName("Backstage").build();
    private final Item conjured = ItemBuilder.anItem().withName("Conjured").build();
    private final Item standard = ItemBuilder.anItem().withName("Standard").build();
    private final Item sulfuras = ItemBuilder.anItem().withName("Sulfuras").build();

    @Before
    public void setUp() {
        itemUpdateScheduledTask = new ItemUpdateScheduledTask(
                itemRepository,
                standardItemUpdateStrategy,
                agedBrieUpdateStrategy,
                backstagePassUpdateStrategy,
                sulfurasUpdateStrategy,
                conjuredItemUpdateStrategy
        );
    }

    @Test
    public void shouldUpdateAgedBrie() throws ValidationException {
        when(itemRepository.findAll()).thenReturn(Collections.singletonList(agedBrie));
        itemUpdateScheduledTask.updateItems();
        verify(agedBrieUpdateStrategy).update(eq(agedBrie));
    }

    @Test
    public void shouldUpdateBackstage() throws ValidationException {
        when(itemRepository.findAll()).thenReturn(Collections.singletonList(backstage));
        itemUpdateScheduledTask.updateItems();
        verify(backstagePassUpdateStrategy).update(eq(backstage));
    }

    @Test
    public void shouldUpdateConjured() throws ValidationException {
        when(itemRepository.findAll()).thenReturn(Collections.singletonList(conjured));
        itemUpdateScheduledTask.updateItems();
        verify(conjuredItemUpdateStrategy).update(eq(conjured));
    }

    @Test
    public void shouldUpdateStandard() throws ValidationException {
        when(itemRepository.findAll()).thenReturn(Collections.singletonList(standard));
        itemUpdateScheduledTask.updateItems();
        verify(standardItemUpdateStrategy).update(eq(standard));
    }

    @Test
    public void shouldUpdateSulfuras() throws ValidationException {
        when(itemRepository.findAll()).thenReturn(Collections.singletonList(sulfuras));
        itemUpdateScheduledTask.updateItems();
        verify(sulfurasUpdateStrategy).update(eq(sulfuras));
    }
}
