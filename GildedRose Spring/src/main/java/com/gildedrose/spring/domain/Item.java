package com.gildedrose.spring.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "gildedrose", type = "user")
public class Item {

    @Id
    public String id;

    public String name;

    public int sellIn;

    public int quality;

    @JsonCreator
    public Item(
            @JsonProperty("id") final String id,
            @JsonProperty("name") final String name,
            @JsonProperty("sellIn") final int sellIn,
            @JsonProperty("quality") final int quality
    ) {
        this.id = id;
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
    }

    @Override
    public String toString() {
        return name + ", " + sellIn + ", " + quality;
    }
}
