package com.gildedrose.spring.domain;

public enum ItemType {
    SULFURAS(80, 0, 0),
    AGED_BRIE(50, -1, 1),
    BACKSTAGE_PASS(50, -1, 1),
    STANDARD(50, -1, -1),
    CONJURED(50, -1, -2);

    public final int minQuality = 0;
    public final int maxQuality;
    public final int sellInStep;
    public final int qualityStep;

    ItemType(final int maxQuality, final int sellInStep, final int qualityStep) {
        this.maxQuality = maxQuality;
        this.sellInStep = sellInStep;
        this.qualityStep = qualityStep;
    }

    public static ItemType ofItem(final Item item) {
        if (item.name.startsWith("Sulfuras")) {
            return SULFURAS;
        } else if (item.name.startsWith("Aged Brie")) {
            return AGED_BRIE;
        } else if (item.name.startsWith("Backstage")) {
            return BACKSTAGE_PASS;
        } else if (item.name.startsWith("Conjured")) {
            return CONJURED;
        }

        return STANDARD;
    }
}
