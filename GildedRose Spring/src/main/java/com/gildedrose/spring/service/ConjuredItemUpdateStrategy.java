package com.gildedrose.spring.service;

import com.gildedrose.spring.domain.Item;
import com.gildedrose.spring.domain.ItemType;
import org.springframework.stereotype.Service;

@Service
public class ConjuredItemUpdateStrategy implements ItemUpdateStrategy {

    public boolean isValid(final Item item) {
        return ItemType.ofItem(item).equals(ItemType.CONJURED) && item.quality <= ItemType.CONJURED.maxQuality;
    }
}
