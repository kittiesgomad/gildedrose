package com.gildedrose.spring.service;

import com.gildedrose.spring.domain.Item;
import com.gildedrose.spring.domain.ItemType;
import org.springframework.stereotype.Service;

@Service
public class AgedBrieUpdateStrategy implements ItemUpdateStrategy {

    public boolean isValid(final Item item) {
        return ItemType.ofItem(item).equals(ItemType.AGED_BRIE) && item.quality <= ItemType.AGED_BRIE.maxQuality;
    }
}
