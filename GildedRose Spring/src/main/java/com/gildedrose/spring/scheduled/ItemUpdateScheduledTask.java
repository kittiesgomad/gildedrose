package com.gildedrose.spring.scheduled;

import com.gildedrose.spring.domain.Item;
import com.gildedrose.spring.domain.ItemType;
import com.gildedrose.spring.repository.ItemRepository;
import com.gildedrose.spring.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.xml.bind.ValidationException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Component
public class ItemUpdateScheduledTask {

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemUpdateScheduledTask.class);

    private final ItemRepository itemRepository;
    private final StandardItemUpdateStrategy standardItemUpdateStrategy;
    private final AgedBrieUpdateStrategy agedBrieUpdateStrategy;
    private final BackstagePassUpdateStrategy backstagePassUpdateStrategy;
    private final SulfurasUpdateStrategy sulfurasUpdateStrategy;
    private final ConjuredItemUpdateStrategy conjuredItemUpdateStrategy;

    @Autowired
    public ItemUpdateScheduledTask(
            final ItemRepository itemRepository,
            final StandardItemUpdateStrategy standardItemUpdateStrategy,
            final AgedBrieUpdateStrategy agedBrieUpdateStrategy,
            final BackstagePassUpdateStrategy backstagePassUpdateStrategy,
            final SulfurasUpdateStrategy sulfurasUpdateStrategy,
            final ConjuredItemUpdateStrategy conjuredItemUpdateStrategy
    ) {
        this.itemRepository = itemRepository;
        this.standardItemUpdateStrategy = standardItemUpdateStrategy;
        this.agedBrieUpdateStrategy = agedBrieUpdateStrategy;
        this.backstagePassUpdateStrategy = backstagePassUpdateStrategy;
        this.sulfurasUpdateStrategy = sulfurasUpdateStrategy;
        this.conjuredItemUpdateStrategy = conjuredItemUpdateStrategy;
    }

    @Scheduled(cron = "0 45 23 * * *") // execute every day at 23:45
    public void updateItems() {
        LOGGER.info(String.format("Starting daily item update. %s", Instant.now()));
        final List<Item> items = new ArrayList<>();
        itemRepository.findAll()
                .forEach(items::add);

        items.stream().parallel()
                .forEach(this::updateItem);
    }

    private void updateItem(final Item item) {
        final ItemType type = ItemType.ofItem(item);
        try {
            updateStrategyForType(type).update(item);
            itemRepository.save(item);
            LOGGER.info(String.format("Updated %s", item.toString()));
        } catch (final ValidationException e) {
            LOGGER.warn(e.getMessage());
        }
    }

    private ItemUpdateStrategy updateStrategyForType(final ItemType type) {
        switch (type) {
            case SULFURAS:
                return sulfurasUpdateStrategy;
            case AGED_BRIE:
                return agedBrieUpdateStrategy;
            case BACKSTAGE_PASS:
                return backstagePassUpdateStrategy;
            case CONJURED:
                return conjuredItemUpdateStrategy;
            default:
                return standardItemUpdateStrategy;
        }
    }
}
