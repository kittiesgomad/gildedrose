package com.gildedrose.spring.controller;

import com.gildedrose.spring.domain.Item;
import com.gildedrose.spring.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/items")
public class ItemController {

    private final ItemRepository itemRepository;

    @Autowired
    public ItemController(final ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @ResponseBody
    @GetMapping("/all")
    public List<Item> items() {
        final List<Item> items = new ArrayList<>();
        itemRepository.findAll()
                .forEach(items::add);

        return items;
    }

}
