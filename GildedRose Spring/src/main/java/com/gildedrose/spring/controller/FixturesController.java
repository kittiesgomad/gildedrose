package com.gildedrose.spring.controller;

import com.gildedrose.spring.domain.builder.ItemBuilder;
import com.gildedrose.spring.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Stream;

@RestController
@RequestMapping("/fixtures")
public class FixturesController {

    private final ItemRepository itemRepository;

    @Autowired
    public FixturesController(final ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @ResponseBody
    @PostMapping()
    public void fixtures() {
        Stream.of(
                ItemBuilder.anItem()
                        .withName("+5 Dexterity Vest")
                        .withSellIn(10)
                        .withQuality(20)
                        .build(),
                ItemBuilder.anItem()
                        .withName("Aged Brie")
                        .withSellIn(2)
                        .withQuality(0)
                        .build(),
                ItemBuilder.anItem()
                        .withName("Elixir of the Mongoose")
                        .withSellIn(5)
                        .withQuality(7)
                        .build(),
                ItemBuilder.anItem()
                        .withName("Sulfuras, Hand of Ragnaros")
                        .withSellIn(0)
                        .withQuality(80)
                        .build(),
                ItemBuilder.anItem()
                        .withName("Sulfuras, Hand of Ragnaros")
                        .withSellIn(-1)
                        .withQuality(80)
                        .build(),
                ItemBuilder.anItem()
                        .withName("Backstage passes to a TAFKAL80ETC concert")
                        .withSellIn(15)
                        .withQuality(20)
                        .build(),
                ItemBuilder.anItem()
                        .withName("Backstage passes to a TAFKAL80ETC concert")
                        .withSellIn(10)
                        .withQuality(49)
                        .build(),
                ItemBuilder.anItem()
                        .withName("Backstage passes to a TAFKAL80ETC concert")
                        .withSellIn(5)
                        .withQuality(49)
                        .build(),
                ItemBuilder.anItem()
                        .withName("Conjured Mana Cake")
                        .withSellIn(3)
                        .withQuality(6)
                        .build()
        )
                .forEach(itemRepository::save);
    }


}
